### Warning: This is POC ###

### What is this repository for? ###

* xml2json provides Flask endpoint for converting XML to JSON and JSON to XML of a provided API endpoint call
* Version 1.0

### How do I get set up? ###

* mkvirtualenv xml2json
* git clone git@bitbucket.org:evilnull/xml2json.git
* pip install -r requirements.txt
* python app.py
* Example call: http://127.0.0.1:5000/json?url=http://www.rtvslo.si/novice/rss/