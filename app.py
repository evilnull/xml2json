#!flask/bin/python
import urllib

from flask import Flask
from flask import request
from flask import Response

from xml2json import xml2json, json2xml

app = Flask(__name__)

class Options(object):
    pass

options = Options()
options.pretty = True

@app.route('/json')
def index_json():
    url = request.args.get('url', 'http://127.0.0.1:5000/example/xml')
    return xml2json(urllib.urlopen(url).read(), options=options)

@app.route('/xml')
def index_xml():
    url = request.args.get('url', 'http://127.0.0.1:5000/example/json')
    return json2xml(urllib.urlopen(url).read())


@app.route('/example/xml')
def example_xml():
    return Response('''<note>
                        <to>Tove</to>
                        <from>Jani</from>
                        <heading>Reminder</heading>
                        <body>Don't forget me this weekend!</body>
                        </note>''',
                    status=200,
                    mimetype='text/xml')


@app.route('/example/json')
def example_json():
    return Response(response='''{"note": {"body": {"#tail": " ", "#text": "Don't forget me this weekend!"}, "to": "Tove", "from": "Jani", "heading": "Reminder"}}''',
                    status=200,
                    mimetype="application/json")

if __name__ == '__main__':
    app.run(debug=True)