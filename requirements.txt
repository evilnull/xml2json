Flask==0.10.1
Jinja2==2.7.3
MarkupSafe==0.23
Werkzeug==0.10.4
argparse==1.2.1
ipython==3.1.0
itsdangerous==0.24
simplejson==3.6.5
wsgiref==0.1.2
xml2json==0.1
